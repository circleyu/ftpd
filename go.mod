module goftp.io/ftpd

go 1.12

require (
	gitea.com/goftp/file-driver v0.0.0-20190711093145-380b52ab0839
	gitea.com/goftp/leveldb-auth v0.0.0-20190711092309-e8e3d5ad5ac8
	gitea.com/goftp/leveldb-perm v0.0.0-20190711092750-00b79e6da99c
	gitea.com/goftp/qiniu-driver v0.0.0-20160416132846-92c10cf5eb34
	gitea.com/lunny/tango v0.6.0
	gitea.com/tango/binding v0.0.0-20190606022902-f7676e2641fd
	gitea.com/tango/flash v0.0.0-20190606021323-2b17fd0aed7c
	gitea.com/tango/renders v0.0.0-20190606020507-5dfc5b32c70d
	gitea.com/tango/session v0.0.0-20190606020146-89f560e05167
	gitea.com/tango/xsrf v0.0.0-20190606015726-fb1b2fb84238
	github.com/Unknwon/goconfig v0.0.0-20190425194916-3dba17dd7b9e
	github.com/goftp/server v0.0.0-20190304020633-eabccc535b5a
	github.com/lunny/log v0.0.0-20160921050905-7887c61bf0de
	github.com/qiniu/api.v6 v6.0.9+incompatible // indirect
	github.com/qiniu/bytes v0.0.0-20140728010635-4887e7b2bde3 // indirect
	github.com/qiniu/rpc v0.0.0-20140728010754-30c22466d920 // indirect
	github.com/syndtr/goleveldb v1.0.0
)
